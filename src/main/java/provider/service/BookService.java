package provider.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import provider.model.Book;

/**
 * BookService
 */
@Service
public class BookService {

    public List<Book> finBooks() {
        return Arrays.asList(new Book(1, "Java", 38.0, "9787513339988"), new Book(2, "Golang", 50.0, "9787513339937"),
                new Book(3, "Scala", 49.0, "9787513339920"));
    }
}
