package provider.model;

import java.util.Date;

/**
 * Book
 */
public class Book {

    private Integer id;
    private String title;
    private Double price;
    private String isbn;
    private Date publishDate;

    public Book(Integer id, String title, Double price, String isbn) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.isbn = isbn;

        this.publishDate = new Date();
    }

    public Book() {

    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(final Double price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(final Date publishDate) {
        this.publishDate = publishDate;
    }

    public void setId(final Integer id) {
        this.id = id;
    }
}
